import React, {Component} from 'react';
import {connect} from 'react-redux';
import {getMe, setScreenSize} from './actions';
import ApplicationBar from '../Menu/ApplicationBar';
import SidePanel from '../Menu/SidePanel';


export class Base extends Component {

    componentDidMount() {
        this.props.getMe();
        window.addEventListener('resize', this.props.setScreenSize);
    }

    render() {
        const {children, user, isDesktop} = this.props;
        if (!user) return null;
        return <div>
            <SidePanel />
            <div style={{marginLeft: isDesktop ? '270px' : '0'}}>
                <ApplicationBar />
                {children}
            </div>
            
        </div>
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.userData.user,
        isDesktop: state.screen.isDesktop
    }   
}

const mapDispatchToProps = (dispatch) => {
    return {
        getMe: () => { dispatch(getMe()) },
        setScreenSize: () => { dispatch(setScreenSize()) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Base);