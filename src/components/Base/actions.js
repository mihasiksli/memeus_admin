import {api} from '../../utils/api';

export function getMe() {
    return (dispatch, getState) => {
        let user = getState().userData.user;
        if (!user) {
            return;
        }
        dispatch({type: 'userData/user/loading'});
        api.get('users/me/').then((response) => {
            let updatedUser = {...user, ...response.data};
            dispatch({type: 'userData/user/set', user: updatedUser});
        }).catch((err) => {});
        
    }
}

export function setScreenSize() {
    return {type: 'screen/size', width: window.innerWidth, height: window.innerHeight};
}