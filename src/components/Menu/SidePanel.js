import React from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import {Link} from 'react-router-dom';
import Drawer from 'material-ui/Drawer';
import MenuItem from 'material-ui/MenuItem';
import AppBar from 'material-ui/AppBar';
import {indigo600 as menuItemActiveBgColor, white as menuItemActiveColor} from 'material-ui/styles/colors';

import {setPanelOpen} from './actions';



export function SidePanel({open, isDesktop, close, location}) {
    let menuItems = [];
    let activeStyle = { backgroundColor: menuItemActiveBgColor, color: menuItemActiveColor };
    return <Drawer docked={isDesktop} open={isDesktop || open} onRequestChange={close}>
        {!isDesktop && <AppBar onLeftIconButtonTouchTap={close}/> }
        
        { menuItems.map((item) => {
            return <MenuItem key={item.name}
                             style={location.pathname === item.defaultUrl ?  activeStyle : {}}
                             onTouchTap={close}
                             primaryText={item.label} 
                             containerElement={<Link to={item.defaultUrl}/>}></MenuItem>
        }) }
    </Drawer>
}

const mapStateToProps = (state) => {
    return {
        isDesktop: state.screen.isDesktop,
        open: state.sidePanel
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        close: () => { dispatch(setPanelOpen(false)) }
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(SidePanel));