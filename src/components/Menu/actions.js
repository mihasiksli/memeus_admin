export function setPanelOpen(open) {
    return {type: 'sidePanel/open', open}
}