import React from 'react';
import {connect} from 'react-redux';
import AppBar from 'material-ui/AppBar';
import {setPanelOpen} from './actions';

export function ApplicationBar({sidePanelOpen, isDesktop, setSidePanelOpen}) {

    if (isDesktop) {
        return null;
    }

    return <AppBar title="Memeus Admin" 
                   onLeftIconButtonTouchTap={() => { setSidePanelOpen(!sidePanelOpen) }}
                   showMenuIconButton={!isDesktop}/>
}

const mapStateToProps = (state) => {
    return {
        isDesktop: state.screen.isDesktop,
        sidePanelOpen: state.sidePanel,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        setSidePanelOpen: (open) => { dispatch(setPanelOpen(open)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ApplicationBar);