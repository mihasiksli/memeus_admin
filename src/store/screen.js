const initialState = {
    width: window.innerWidth,
    height: window.innerHeight,
    isDesktop: window.innerWidth >= 960,
}

export default function screen(state=initialState, action) {
    switch(action.type) {
        case ('screen/size'):
            let width = parseInt(action.width, 10) || 0,
            height = parseInt(action.height, 10) || 0,
            isDesktop = width >= 960;
            return {...state, width, height, isDesktop};
        default:
            return state;
    }
}