import {createStore, combineReducers, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';

import userData from './user';
import sidePanel from './sidePanel';
import screen from './screen';


const middleware = applyMiddleware(thunk);
const reducers = combineReducers({userData, sidePanel, screen});

export const store = createStore(reducers, middleware);