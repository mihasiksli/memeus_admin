import cookie from 'js-cookie';
import jsonwebtoken from 'jsonwebtoken';



const jwt = cookie.get('jwt');
const user = jwt ? jsonwebtoken.decode(jwt) : null;

const initialState = {
    user,
    error: false,
    loading: false,
    fullData: true,
};

export default function userData(state=initialState, action) {
    switch(action.type) {
        case 'userData/user/set':
            return {...state, user: {...action.user}, loading: false, fullData: true};
        default:
            return state;
    }
    
}