

export default function sidePanel(state=false, action) {
    switch(action.type) {
        case('sidePanel/open'):
            return !!action.open;
        default:
            return state;
    }
}