import React, { Component } from 'react';
import {Provider} from 'react-redux';
import {store} from './store/store';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import Base from './components/Base/Base';
import IndexPage from './components/IndexPage/IndexPage';

class App extends Component {
    render() {
        return (
        <Provider store={store}>
        <BrowserRouter>
            <MuiThemeProvider>
                <div className="App">
                <Base>
                    <Switch>
                        <Route exact path='/' component={IndexPage} />
                    </Switch>
                </Base>
                </div>
        
            </MuiThemeProvider>
        </BrowserRouter>
        </Provider>
        );
    }
}

export default App;
