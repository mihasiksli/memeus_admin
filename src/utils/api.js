import axios from 'axios';
import cookie from 'js-cookie';

let interceptor = function(config) {
    let jwt = cookie.get('jwt');
    config.headers['Authorization'] = 'Bearer ' + jwt;
    return config;
};

export let api = axios.create({baseURL: process.env.REACT_APP_API_URL } );

api.interceptors.request.use(interceptor);
